#!/usr/bin/python
from jinja2 import Environment, PackageLoader

from core.handler import Handler


def application(env, start_response):
    jinja_env = Environment(loader=PackageLoader('app', 'static'))
    handler = Handler(env, jinja_env)
    response = handler.process()

    start_response(response['status'], response['header'])
    return response['content']