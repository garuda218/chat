class RouteNotFoundError(Exception):
    pass


class Router(object):
    def __init__(self, routes=None):
        self.__routes = {}
        if routes:
            self.add_routes(routes)

    def __setitem__(self, key, value):
        self.add_route(key, value)

    def __getitem__(self, url):
        return self.find_route(url)

    def find_route(self, url):
        try:
            route = self.__routes[url]
        except KeyError:
            raise RouteNotFoundError()

        return route

    def add_route(self, url, method):
        """
        Add single route

        :param url: URL string
        :param method: Method called by specified URL
        """
        self.__routes[url] = method

    def add_routes(self, routes):
        """
        Add multiple routes

        :param routes: Routes in project
        """
        for url, method in routes.iteritems():
            self.add_route(url, method)
