import front_handler
import handler


from router import RouteNotFoundError


class ChannelHandler(object):
    def __init__(self, name):
        self.name = name
        self.ch_url = '/channel/' + self.name

    def create_channel(self):
        try:
            handler.Handler.route_handler.find_route(self.ch_url)
        except RouteNotFoundError:
            handler.Handler.route_handler.add_route(self.ch_url, front_handler.Channel)

        return {'status': '301 Redirect',
                'header': [('Location', self.ch_url)],
                'content': ''}

    def connect_channel(self):
        handler.Handler.route_handler.find_route(self.ch_url)

        return {'status': '301 Redirect',
                'header': [('Location', self.ch_url)],
                'content': ''}