class Front(object):
    template = None

    def __init__(self, env, jinja_env):
        self.__jinja_env = jinja_env
        self.__template = self.__jinja_env.get_template(self.template)
        self.__env = env

    def render_template(self):
        return {'status': '200 OK',
                'header': [('Content-type', 'text/html')],
                'content': self.__template.render(
                    host=self.__env['HTTP_HOST']
                ).encode('utf-8')}


class MainPage(Front):
    template = 'entrance.html'


class Registration(Front):
    template = 'registration.html'


class Channel(Front):
    template = 'chat-page.html'


class Lobby(Front):
    template = 'lobby.html'


class Error404(Front):
    def render_template(self):
        return {'status': '404 Not found',
                'header': [('Content-type', 'text/html')],
                'content': 'Page not found'}