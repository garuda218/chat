import cgi
import datetime
import json
import redis

import base_model


class Chat(object):
    response = {'status': '200 OK',
                'header': [('Content-Type', 'application/json')],
                'content': json.dumps({'status': 'success'})}
    msg_load = False

    @classmethod
    def save_message(cls, params, ch_name):
        db = base_model.BaseModel.db

        params['time'] = datetime.datetime.now()
        params['message'] = cgi.escape(params['message'])

        msg = '{}: {}'.format(params['time'], params['message'])

        db.set('message', msg)

        cls.msg_load = True

        return cls.response

    @classmethod
    def get_message(cls):
        if cls.msg_load is True:
            db = base_model.BaseModel.db
            msg = db.get('message')
            cls.msg_load = False
            cls.response['content'] = json.dumps(
            {'status': 'success',
             'message': json.dumps(msg)}
            )
        else:
            cls.response['content'] = json.dumps(
            {'status': 'fail',
             'message': json.dumps('none')}
            )

        return cls.response