import cgi
import urlparse

import channel
import chat
import front_handler
import user

from router import Router, RouteNotFoundError


class Handler(object):
    routes = {
        '/': front_handler.MainPage,
        '/register': front_handler.Registration,
        '/channel': front_handler.Channel,
        '/lobby': front_handler.Lobby
    }
    route_handler = Router(routes)

    def __init__(self, env, jinja_env):
        self.__env = env
        self.__jinja_env = jinja_env

    def process(self):
        if self.__env['REQUEST_METHOD'] == 'POST':
            return self.__handle_post()

        try:
            return self.route_handler[self.__env['PATH_INFO']](
                self.__env, self.__jinja_env
            ).render_template()
        except RouteNotFoundError:
            return {'status': '404 Not found',
                    'header': [('Content-type', 'text/plain')],
                    'content': 'Page not found'
                    }

    def __handle_post(self):
        try:
            request_body_size = int(self.__env.get('CONTENT_LENGTH', 0))
        except ValueError:
            request_body_size = 0
        request_body = self.__env['wsgi.input'].read(request_body_size)
        data = urlparse.parse_qs(request_body)
        data = {k: data[k][0] for k in data.keys()}

        print(data)
        if self.__env['PATH_INFO'] == '/getmessage':
            return chat.Chat.get_message()
        if self.__env['PATH_INFO'] == '/sendmessage':
            return chat.Chat.save_message(data, self.__env['PATH_INFO'])
        if self.__env['PATH_INFO'] == '/auth':
            return user.User.login(data)
        if self.__env['PATH_INFO'] == '/reguser':
            return user.User.reg_user(data)
        if self.__env['PATH_INFO'] == '/create':
            return channel.ChannelHandler(data['channel-name']).create_channel()
        if self.__env['PATH_INFO'] == '/connect':
            return channel.ChannelHandler(data['channel-name']).connect_channel()