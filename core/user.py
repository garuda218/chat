import cgi


import base_model
import front_handler


class LoginError(Exception):
    pass


class PasswordError(Exception):
    pass


class UserExistsError(Exception):
    pass


class User(base_model.BaseModel):
    response = {'status': '301 Redirect',
                'header': [('Location', '/lobby')],
                'content': ''}

    @classmethod
    def __escape_args(cls, args):
        for k in args:
            args[k] = cgi.escape(args[k])
        return args

    @classmethod
    def __validate_login(cls, args):
        if cls.db.get(args['login'].lower()) is None:
            raise LoginError()
        if cls.db.get(args['login'].lower()) != args['password']:
            raise PasswordError()

    @classmethod
    def login(cls, args):
        args = cls.__escape_args(args)

        cls.__validate_login(args)
        # TODO: handle Exceptions

        return cls.response

    @classmethod
    def reg_user(cls, args):
        if cls.db.exists(args['login'].lower()) is True:
            raise UserExistsError()

        cls.db.set(args['login'].lower(), args['password'].lower())

        return cls.response

        # TODO: create password security


    # def proceed(self):
    #     response = dict()
    #
    #     response['status'] = '200 OK'
    #
    #     try:
    #         self.__validation()
    #     except Exception:
    #         response['header'] = [('Content-Type', 'application/json')]
    #         response['content'] = json.dumps({'status': 'fail',
    #                                           'msg': 'login fail'
    #                                           })
    #
    #     response['header'] = [('Content-Type', 'text/html')]
    #     response['content'] = json.dumps({'status': 'success',
    #                                       'url': '/lobby'
    #     })
    #     return response